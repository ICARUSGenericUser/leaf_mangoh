	# Mocca Red Makefile
#

UPDATE_FILE_DIR = build/update_files
LEAF_DATA ?= $(LEAF_WORKSPACE)/leaf-data/current

# Arguments passed to mksys whenever it is invoked.
MKSYS_ARGS_COMMON = -w $(1) --cflags=-O2 --object-dir=build/$@ --output-dir=$(UPDATE_FILE_DIR)

MAKEFILE_DIR := $(dir $(realpath $(firstword $(MAKEFILE_LIST))))

# List of all supported mangOH boards.
BOARDS = red

# List of all supported Sierra Wireless WP-series modules.
MODULES = wp76xx

# List of goals for each board, in the form $(board)_$(module), like green_wp85.
RED_GOALS = $(foreach module,$(MODULES),red_$(module))

# Build goals that don't rely on LEGATO_TARGET, instead allowing the module to be specified
# on the command line as part of the goal. E.g., 'make green_wp85'.  If using these, Legato's
# findtoolchain script will be used to try to find the approriate build toolchain.

$(RED_GOALS): red_%:
# NOTE: When using leaf, these TOOLCHAIN_X variables don't need to be passed to mksys.
	mksys -t $* $(MKSYS_ARGS_COMMON) red_wp76.sdef

# The cleaning goal.
.PHONY: clean
clean:
	rm -rf build