#!/bin/bash
scp -r openjre8 root@192.168.2.2:.
scp profile root@192.168.2.2:.profile
ssh root@192.168.2.2 << EOF
mkdir bin
cd bin
ln -s ~/openjre8/bin/java java
cd
java -version
EOF
