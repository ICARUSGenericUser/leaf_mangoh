#!/bin/bash
scp -r oracle-ejdk-compact2 root@192.168.2.2:.
scp profile root@192.168.2.2:.profile
ssh root@192.168.2.2 << EOF
mkdir bin
cd bin
ln -s ~/oracle-ejdk-compact2/bin/java java
cd
java -version
EOF
